# sfaddress-hk
simple tools that help you get sf-exress hk address.

## Installation
goto your project and enter the command:
` npm i sfaddress-hk `

## Initial
```js
  // create the base objs
  const sf = require('sfaddress-hk')

  // init the sfaddress-hk from dafault csv
  const sfaddress = sf()

  // or assign csv from this module included
  const csvKeys = sf().csv
  const sfaddress = sf(csvKeys.store_en) // SF Store (English)
  const sfaddress = sf(csvKeys.store_zh) // SF Store (Chinese)
  const sfaddress = sf(csvKeyss.locker_zh) // Self-pick points from SF locker (Chinese)  

  // Or, you can read the csv file follow the format
  // If the district is empty, it will use the previous district.
  // "district", "code" , "address", "weekday" , "weekend"
  // "香港仔",852TAL,"香港香港仔大道 234 號富嘉工業大廈 9 樓 6 室","11:00-22:00","12:00-20:00"
  // "",852TBL,"香港薄扶林華富道18號華富(一)邨華安樓119號舖","11:00-22:00","12:00-20:00"
  // "鴨脷洲",852TCL,"香港鴨脷洲鴨脷洲大街42-44號地下1號舖","11:00-22:00","12:00-20:00"
  // .....
  const sfaddress = require('sfaddress-hk')(your_custom_csv)
```

## Usage
```js
  // Get all store as JSONArray:
  sfaddress.allStore // array[object]

  // Get all available district:
  sfaddress.allDistrict // array[string]

  // Search sf-point / store by district (need match exactly):
  sfaddress.searchByDistrict('香港仔') //return array[object]

  // Get a store details by use the code:
  sfaddress.getByCode('852TAL') // return object

  // Search sf-point / store by searching the address:
  sfaddress.searchInAddress('地下')

  //Or, using RegExp:
  sfaddress.searchInAddress(/[A-Z]/g)

```

## Extra Parameter
```js
  // Get module dirname:
  sfaddress.dirname

  //get the path of index.js of this module
  sfaddress.filename
```

## License
address infomation is owned by SF-Express.
the other code is licensed under ISC:
```
Copyright 2018-, Len Chan<lenchan139@tto.moe>

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
```
