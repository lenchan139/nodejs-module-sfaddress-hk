const csv=require("csv-load-sync")

var storeJson = []
module.exports = function(inPath){
  if(inPath){
    storeJson = csv(inPath)
  }else{
    storeJson = csv(__dirname + '/csv/store-zh.csv')
  }
  let result = {}
  result.allStore =  getAllStore()
  result.allDistrict = getAllDistrict()
  result.searchByDistrict = searchByDistrict
  result.getByCode = getByCode
  result.searchInAddress = searchInAddress
  result.csv = getCsvFilenames()
  result.filename = __filename
  result.dirname = __dirname
  return result
}
function getCsvFilenames(){
  return {
    locker_zh: __dirname + '/csv/locker-zh.csv',
    store_en: __dirname + '/csv/store-en.csv',
    store_zh: __dirname + '/csv/store-zh.csv'
  }
}
function getAllStore(){
  let resultArray = []
  let lastDistrict
  for(i in storeJson){
    let c = storeJson[i]
    let obj = {}
    if(c.district && c.district != "")
      lastDistrict = c.district
    obj.district = lastDistrict
    obj.code = c.code
    obj.address = c.address
    obj.weekday = c.weekday
    obj.weekend = c.weekend
    resultArray.push(obj)
  }
  return resultArray
}

function getAllDistrict(){
  let resultArray = []
  let lastDistrict
  let allStore = getAllStore()
  for(i in allStore){
    let c = allStore[i]
    let obj = {}
    let district = c.district
    if(!resultArray.includes(district)){
      resultArray.push(district)
    }
  }
  return resultArray
}

function searchByDistrict(district){
  let resultArray = []
  let allStore = getAllStore()
  for(i in allStore){
    let c = allStore[i]
    if(c.district == district){
      resultArray.push(c)
    }
  }
  return resultArray
}
function getByCode(code){
  let resultArray = []
  let allStore = getAllStore()
  for(i in allStore){
    let c = allStore[i]
    if(c.code == code){
      return c
    }
  }
}
function searchInAddress(keyword){
  let resultArray = []
  let allStore = getAllStore()
  for(i in allStore){
    let c = allStore[i]
    let isInclude = false
    if(typeof(keyword) == "string"){
      isInclude = c.address.includes(keyword)
    }else{
      isInclude = Boolean(c.address.match(keyword))
    }
    if(isInclude){
      resultArray.push(c)
    }
  }
  return resultArray
}
